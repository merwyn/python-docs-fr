# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-15 22:33+0100\n"
"PO-Revision-Date: 2022-06-06 21:29-0400\n"
"Last-Translator: Nicolas Haller <nicolas@haller.im>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0.1\n"

#: library/asyncio-dev.rst:7
msgid "Developing with asyncio"
msgstr "Programmer avec *asyncio*"

#: library/asyncio-dev.rst:9
msgid ""
"Asynchronous programming is different from classic \"sequential\" "
"programming."
msgstr ""
"La programmation asynchrone est différente de la programmation "
"« séquentielle » classique."

#: library/asyncio-dev.rst:12
msgid ""
"This page lists common mistakes and traps and explains how to avoid them."
msgstr ""
"Cette page liste les pièges et erreurs communs que le développeur pourrait "
"rencontrer et décrit comment les éviter."

#: library/asyncio-dev.rst:19
msgid "Debug Mode"
msgstr "Mode débogage"

#: library/asyncio-dev.rst:21
msgid ""
"By default asyncio runs in production mode.  In order to ease the "
"development asyncio has a *debug mode*."
msgstr ""
"Par défaut, *asyncio* s'exécute en mode production. Pour faciliter le "
"développement, *asyncio* possède un « mode débogage »."

#: library/asyncio-dev.rst:24
msgid "There are several ways to enable asyncio debug mode:"
msgstr "Il existe plusieurs façons d'activer le mode débogage de *asyncio* :"

#: library/asyncio-dev.rst:26
msgid "Setting the :envvar:`PYTHONASYNCIODEBUG` environment variable to ``1``."
msgstr ""
"en réglant la variable d’environnement :envvar:`PYTHONASYNCIODEBUG` à ``1`` ;"

#: library/asyncio-dev.rst:28
msgid "Using the :ref:`Python Development Mode <devmode>`."
msgstr ""
"en utilisant le mode développement de Python (:ref:`Python Development Mode "
"<devmode>`) ;"

#: library/asyncio-dev.rst:30
msgid "Passing ``debug=True`` to :func:`asyncio.run`."
msgstr "en passant ``debug=True`` à la fonction :func:`asyncio.run` ;"

#: library/asyncio-dev.rst:32
msgid "Calling :meth:`loop.set_debug`."
msgstr "en appelant la méthode :meth:`loop.set_debug`."

#: library/asyncio-dev.rst:34
msgid "In addition to enabling the debug mode, consider also:"
msgstr "En plus d'activer le mode débogage, vous pouvez également :"

#: library/asyncio-dev.rst:36
msgid ""
"setting the log level of the :ref:`asyncio logger <asyncio-logger>` to :py:"
"data:`logging.DEBUG`, for example the following snippet of code can be run "
"at startup of the application::"
msgstr ""
"régler le niveau de journalisation pour l'enregistreur d'*asyncio* (:ref:"
"`asyncio logger <asyncio-logger>`) à :py:data:`logging.DEBUG` ; par exemple, "
"le fragment de code suivant peut être exécuté au démarrage de "
"l'application ::"

#: library/asyncio-dev.rst:42
msgid ""
"configuring the :mod:`warnings` module to display :exc:`ResourceWarning` "
"warnings.  One way of doing that is by using the :option:`-W` ``default`` "
"command line option."
msgstr ""
"configurer le module :mod:`warnings` afin d'afficher les avertissements de "
"type :exc:`ResourceWarning` ; vous pouvez faire cela en utilisant l'option :"
"option:`-W` ``default`` sur la ligne de commande."

#: library/asyncio-dev.rst:47
msgid "When the debug mode is enabled:"
msgstr "Lorsque le mode débogage est activé :"

# long ref
#: library/asyncio-dev.rst:49
msgid ""
"asyncio checks for :ref:`coroutines that were not awaited <asyncio-coroutine-"
"not-scheduled>` and logs them; this mitigates the \"forgotten await\" "
"pitfall."
msgstr ""
"*asyncio* surveille les :ref:`coroutines qui ne sont jamais attendues "
"<asyncio-coroutine-not-scheduled>` et les journalise ; cela atténue le "
"problème des « *await* oubliés » ;"

# thread safe comment traduire
#: library/asyncio-dev.rst:53
msgid ""
"Many non-threadsafe asyncio APIs (such as :meth:`loop.call_soon` and :meth:"
"`loop.call_at` methods) raise an exception if they are called from a wrong "
"thread."
msgstr ""
"beaucoup d'*API* *asyncio* ne prenant pas en charge les fils d'exécution "
"multiples (comme les méthodes :meth:`loop.call_soon` et :meth:`loop."
"call_at`) lèvent une exception si elles sont appelées par le mauvais fil "
"d’exécution ;"

#: library/asyncio-dev.rst:57
msgid ""
"The execution time of the I/O selector is logged if it takes too long to "
"perform an I/O operation."
msgstr ""
"le temps d'exécution du sélecteur d'entrée-sortie est journalisé si une "
"opération prend trop de temps à s'effectuer ;"

#: library/asyncio-dev.rst:60
msgid ""
"Callbacks taking longer than 100 milliseconds are logged.  The :attr:`loop."
"slow_callback_duration` attribute can be used to set the minimum execution "
"duration in seconds that is considered \"slow\"."
msgstr ""
"les fonctions de rappel prenant plus de 100 ms sont journalisées ; "
"l'attribut :attr:`loop.slow_callback_duration` peut être utilisé pour "
"changer la limite (en secondes) après laquelle une fonction de rappel est "
"considérée comme « lent »."

#: library/asyncio-dev.rst:68
msgid "Concurrency and Multithreading"
msgstr "Programmation concurrente et multi-fils"

#: library/asyncio-dev.rst:70
msgid ""
"An event loop runs in a thread (typically the main thread) and executes all "
"callbacks and Tasks in its thread.  While a Task is running in the event "
"loop, no other Tasks can run in the same thread.  When a Task executes an "
"``await`` expression, the running Task gets suspended, and the event loop "
"executes the next Task."
msgstr ""
"Une boucle d'évènements s'exécute dans un fil d’exécution (typiquement dans "
"le fil principal) et traite toutes les fonctions de rappel (*callbacks*) "
"ainsi que toutes les tâches dans ce même fil. Lorsqu'une tâche est en cours "
"d'exécution dans la boucle d'évènements, aucune autre tâche ne peut "
"s'exécuter dans ce fil. Quand une tâche traite une expression ``await``, "
"elle se suspend et laisse la boucle d’évènements traiter la tâche suivante."

#: library/asyncio-dev.rst:76
msgid ""
"To schedule a :term:`callback` from another OS thread, the :meth:`loop."
"call_soon_threadsafe` method should be used. Example::"
msgstr ""
"Pour planifier un :term:`rappel <callback>` depuis un autre fil d'exécution "
"système, utilisez la méthode :meth:`loop.call_soon_threadsafe`. Par "
"exemple ::"

#: library/asyncio-dev.rst:81
msgid ""
"Almost all asyncio objects are not thread safe, which is typically not a "
"problem unless there is code that works with them from outside of a Task or "
"a callback.  If there's a need for such code to call a low-level asyncio "
"API, the :meth:`loop.call_soon_threadsafe` method should be used, e.g.::"
msgstr ""
"La plupart des objets *asyncio* ne sont pas conçus pour être exécutés dans "
"un contexte multi-fils (*thread-safe*) mais cela n'est en général pas un "
"problème à moins que l'objet ne fasse appel à du code se trouvant en dehors "
"d'une tâche ou d'une fonction de rappel. Dans ce dernier cas, si le code "
"appelle les *API* bas niveau de *asyncio*, utilisez la méthode :meth:`loop."
"call_soon_threadsafe`. Par exemple ::"

#: library/asyncio-dev.rst:89
msgid ""
"To schedule a coroutine object from a different OS thread, the :func:"
"`run_coroutine_threadsafe` function should be used. It returns a :class:"
"`concurrent.futures.Future` to access the result::"
msgstr ""
"Pour planifier un objet concurrent depuis un autre fil d'exécution système, "
"utilisez :func:`run_coroutine_threadsafe`. Cette fonction renvoie un objet :"
"class:`concurrent.futures.Future` pour accéder au résultat ::"

#: library/asyncio-dev.rst:102
msgid ""
"To handle signals and to execute subprocesses, the event loop must be run in "
"the main thread."
msgstr ""
"Pour pouvoir traiter les signaux et démarrer des processus enfants, la "
"boucle d'évènements doit être exécutée dans le fil principal."

#: library/asyncio-dev.rst:105
msgid ""
"The :meth:`loop.run_in_executor` method can be used with a :class:"
"`concurrent.futures.ThreadPoolExecutor` to execute blocking code in a "
"different OS thread without blocking the OS thread that the event loop runs "
"in."
msgstr ""
"La méthode :meth:`loop.run_in_executor` peut être utilisée avec :class:"
"`concurrent.futures.ThreadPoolExecutor` pour exécuter du code bloquant dans "
"un autre fil d'exécution, afin de ne pas bloquer le fil où la boucle "
"d'évènements se trouve."

#: library/asyncio-dev.rst:110
#, fuzzy
msgid ""
"There is currently no way to schedule coroutines or callbacks directly from "
"a different process (such as one started with :mod:`multiprocessing`). The :"
"ref:`asyncio-event-loop-methods` section lists APIs that can read from pipes "
"and watch file descriptors without blocking the event loop. In addition, "
"asyncio's :ref:`Subprocess <asyncio-subprocess>` APIs provide a way to start "
"a process and communicate with it from the event loop. Lastly, the "
"aforementioned :meth:`loop.run_in_executor` method can also be used with a :"
"class:`concurrent.futures.ProcessPoolExecutor` to execute code in a "
"different process."
msgstr ""
"Il n'y a actuellement aucune façon de planifier des coroutines ou des "
"rappels directement depuis un autre processus (comme, par exemple, un "
"processus démarré avec :mod:`multiprocessing`). La section :ref:`Méthodes de "
"la boucle d'évènements <asyncio-event-loop>` liste les *API* pouvant lire "
"les tubes (*pipes*) et surveiller les descripteurs de fichiers sans bloquer "
"la boucle d'évènements. De plus, les *API* :ref:`Subprocess <asyncio-"
"subprocess>` d'*asyncio* fournissent un moyen de démarrer un processus et de "
"communiquer avec lui depuis la boucle d'évènements. Enfin, la méthode :meth:"
"`loop.run_in_executor` peut également être utilisée avec :class:`concurrent."
"futures.ProcessPoolExecutor` pour exécuter du code dans un processus "
"différent."

#: library/asyncio-dev.rst:124
msgid "Running Blocking Code"
msgstr "Exécution de code bloquant"

#: library/asyncio-dev.rst:126
msgid ""
"Blocking (CPU-bound) code should not be called directly.  For example, if a "
"function performs a CPU-intensive calculation for 1 second, all concurrent "
"asyncio Tasks and IO operations would be delayed by 1 second."
msgstr ""
"Du code bloquant sur des opérations de calcul (*CPU-bound*) ne devrait pas "
"être appelé directement. Par exemple, si une fonction effectue des calculs "
"utilisant le CPU intensivement pendant une seconde, toutes les tâches "
"*asyncio* concurrentes et les opérations d'entrées-sorties seront bloquées "
"pour une seconde."

#: library/asyncio-dev.rst:131
msgid ""
"An executor can be used to run a task in a different thread or even in a "
"different process to avoid blocking the OS thread with the event loop.  See "
"the :meth:`loop.run_in_executor` method for more details."
msgstr ""
"Un exécuteur peut être utilisé pour traiter une tâche dans un fil "
"d'exécution ou un processus différent, afin d'éviter de bloquer le fil "
"d'exécution système dans lequel se trouve la boucle d’évènements. Voir :meth:"
"`loop.run_in_executor` pour plus de détails."

#: library/asyncio-dev.rst:140
msgid "Logging"
msgstr "Journalisation"

#: library/asyncio-dev.rst:142
msgid ""
"asyncio uses the :mod:`logging` module and all logging is performed via the "
"``\"asyncio\"`` logger."
msgstr ""
"*Asyncio* utilise le module :mod:`logging`. Toutes les opérations de "
"journalisation sont effectuées via l'enregistreur (*logger*) ``\"asyncio\"``."

#: library/asyncio-dev.rst:145
msgid ""
"The default log level is :py:data:`logging.INFO`, which can be easily "
"adjusted::"
msgstr ""
"Le niveau de journalisation par défaut est :py:data:`logging.INFO` mais peut "
"être ajusté facilement ::"

#: library/asyncio-dev.rst:151
msgid ""
"Network logging can block the event loop. It is recommended to use a "
"separate thread for handling logs or use non-blocking IO. For example, see :"
"ref:`blocking-handlers`."
msgstr ""

#: library/asyncio-dev.rst:159
msgid "Detect never-awaited coroutines"
msgstr "Détection des coroutines jamais attendues"

#: library/asyncio-dev.rst:161
msgid ""
"When a coroutine function is called, but not awaited (e.g. ``coro()`` "
"instead of ``await coro()``) or the coroutine is not scheduled with :meth:"
"`asyncio.create_task`, asyncio will emit a :exc:`RuntimeWarning`::"
msgstr ""
"Lorsqu'une fonction coroutine est appelée mais qu'elle n'est pas attendue "
"(p. ex.  ``coro()`` au lieu de ``await coro()``) ou si la coroutine n'est "
"pas planifiée avec :meth:`asyncio.create_task`, *asyncio* émet un :exc:"
"`RuntimeWarning` ::"

#: library/asyncio-dev.rst:221
msgid "Output::"
msgstr "Sortie ::"

#: library/asyncio-dev.rst:237
msgid "Output in debug mode::"
msgstr "Affichage en mode débogage ::"

#: library/asyncio-dev.rst:194
msgid ""
"The usual fix is to either await the coroutine or call the :meth:`asyncio."
"create_task` function::"
msgstr ""
"La façon habituelle de régler ce problème est d'attendre (*await*) la "
"coroutine ou bien d'appeler la fonction :meth:`asyncio.create_task` ::"

#: library/asyncio-dev.rst:202
msgid "Detect never-retrieved exceptions"
msgstr "Détection des exceptions jamais récupérées"

#: library/asyncio-dev.rst:204
msgid ""
"If a :meth:`Future.set_exception` is called but the Future object is never "
"awaited on, the exception would never be propagated to the user code.  In "
"this case, asyncio would emit a log message when the Future object is "
"garbage collected."
msgstr ""
"Si la méthode :meth:`Future.set_exception` est appelée mais que l'objet "
"*Future* n'est pas attendu, l'exception n'est pas propagée au code "
"utilisateur. Dans ce cas, *asyncio* écrit un message dans le journal lorsque "
"l'objet *Future* est récupéré par le ramasse-miette."

#: library/asyncio-dev.rst:209
msgid "Example of an unhandled exception::"
msgstr "Exemple d'une exception non-gérée ::"

#: library/asyncio-dev.rst:232
msgid ""
":ref:`Enable the debug mode <asyncio-debug-mode>` to get the traceback where "
"the task was created::"
msgstr ""
":ref:`Activez le mode débogage <asyncio-debug-mode>` pour récupérer la trace "
"d'appels indiquant où la tâche a été créée ::"
